<?php
/**
 * Lets keep this nice and short. 
 * 
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.rst
 * Redistributions of files must retain the above copyright notice.
 *
 * @author Roman Tomjak <r.tomjaks@gmail.com>
 * @copyright 2014 Roman Tomjak
 * @link https://bitbucket.org/r00m/grave
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 * @package grave
 */

// comment out the following line when deployed to production
defined('GRAVE_DEBUG') or define('GRAVE_DEBUG', true);

require(__DIR__ . '/vendor/autoload.php');

new Grave\Web\Application();