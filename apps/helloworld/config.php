<?php
/**
 * Application configuration.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.rst
 * Redistributions of files must retain the above copyright notice.
 * 
 * @author Roman Tomjak <r.tomjaks@gmail.com>
 * @copyright 2014 Roman Tomjak
 * @link https://bitbucket.org/r00m/grave
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 * @package grave
 */

return array(
    // api key
    'api_key' => sha1("my cool app"),
    
    // whether to send CORS headers
    'enable_cors' => false,
    
    // db credentials
    /*'db' => array(
        'host' => 'localhost',
        'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    )*/
);
