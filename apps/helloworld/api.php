<?php
/**
 * Here we define API endpoints (routes) that are accessible through specified
 * namespaces and request methods.
 * 
 * E.g. http://api.com/v1/foo, http://api.com/v2/bar, etc.
 * 
 * For example, a route, accessable through GET and POST requests,
 * with a namespace of '/v1/', would be defined like the following:
 *  /**
 *   * @namespace /v1
 *   * @url /foo_bar_route/:id
 *   * @via GET,POST
 *   * /
 *  public function this_can_be_random() { }
 * 
 * ... and publicly accessable URL would be: http://api.com/v1/foo_bar_route/12
 * 
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.rst
 * Redistributions of files must retain the above copyright notice.
 * 
 * @author Roman Tomjak <r.tomjaks@gmail.com>
 * @copyright 2014 Roman Tomjak
 * @link https://bitbucket.org/r00m/grave
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 * @package grave
 */

class API
{
    /**
     * Provides access to application's configuration
     * 
     * @var array 
     */
    public $config;

    /**
     * This example responds from http://api.com/v1/hello
     * 
     * Here you can also see how you can use underlying framework's (Slim) features
     * 
     * @namespace /v1
     * @url /hello
     * @via GET
     */
    public function hello_world()
    {
        $slim = \Slim\Slim::getInstance();

        $ret = array(
            'hello' => 'world!',
            'image' => $slim->request->getScheme() . "://{$_SERVER['HTTP_HOST']}/assets/images/php_elephants.jpg",
            'ip' => $slim->request->getIp(),
        );

        echo json_encode($ret);
    }
    
    /**
     * And this example responds from http://api.com/v2/hello
     * 
     * This example is stacked with data from a custom method
     * 
     * @namespace /v2
     * @url /hello
     * @via GET
     */
    public function hello_world_v2()
    {
        $ret = array(
            'hello' => 'world!',
            'version' => 2,
            'custom' => $this->custom_method()
        );

        echo json_encode($ret);
    }
    
    /**
     * Custom method that does some work
     * 
     * @return string
     */
    private function custom_method()
    {
        return 'method';
    }
    
}