<?php
/**
 * CLI interface for creating/listing Grave apps.
 * 
 * This script is meant to be run from command line to
 * execute one of the pre-defined commands.
 * 
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.rst
 * Redistributions of files must retain the above copyright notice.
 *
 * @author Roman Tomjak <r.tomjaks@gmail.com>
 * @copyright 2014 Roman Tomjak
 * @link https://bitbucket.org/r00m/grave
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 * @package grave
 */

namespace Grave\Console;

class Application extends \Grave\Base\Application
{    
    /**
     * Warnings or errors that occured when CLI client is running
     * 
     * @var array 
     */
    private $warnings = array();
    
    /**
     * Template for api.php
     * 
     * @var string 
     */
    private $api_template = <<< 'EOF'
<?php
/**
 * Here we define API endpoints (routes) that are accessible through specified
 * namespaces and request methods.
 * 
 * E.g. http://api.com/v1/foo, http://api.com/v2/bar, etc.
 * 
 * For example, a route, accessable through GET and POST requests,
 * with a namespace of '/v1/', would be defined like the following:
 *  /**
 *   * @namespace /v1
 *   * @url /foo_bar_route/:id
 *   * @via GET,POST
 *   * /
 *  public function this_can_be_random() { }
 * 
 * ... and publicly accessable URL would be: http://api.com/v1/foo_bar_route/12
 * 
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.rst
 * Redistributions of files must retain the above copyright notice.
 * 
 * @author Roman Tomjak <r.tomjaks@gmail.com>
 * @copyright 2014 Roman Tomjak
 * @link https://bitbucket.org/r00m/grave
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 * @package grave
 */

class API
{
    /**
     * Provides access to application's configuration
     * 
     * @var array 
     */
    public $config;

    /**
     * Hello world example!
     * 
     * This URL can only be accessed through
     * GET requests at: http://api.com/v1/hello
     *
     * @namespace /v1
     * @url /hello
     * @via GET
     */
    public function hello_world()
    {
        $ret = array(
            'hello' => 'world!'
        );

        echo json_encode($ret);
    }
    
}
EOF;
    
    /**
     * Template for config.php
     * 
     * @var string 
     */
    private $config_template = <<< 'EOF'
<?php
/**
 * Application configuration.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.rst
 * Redistributions of files must retain the above copyright notice.
 * 
 * @author Roman Tomjak <r.tomjaks@gmail.com>
 * @copyright 2014 Roman Tomjak
 * @link https://bitbucket.org/r00m/grave
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 * @package grave
 */

return array(
    // api key
    'api_key' => 'KEY_PLACEHOLDER',
    
    // whether to send CORS headers
    'enable_cors' => false,
    
    // db credentials
    /*'db' => array(
        'host' => 'localhost',
        'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    )*/
);
EOF;
    
    public function __construct($argc, $argv)
    {
        parent::__construct(false);
        
        ob_start();
        
        if ($argc <= 1)
        {
            $this->showUsage();
        }
        else
        {
            $method = 'command' . ucfirst($argv[1]);
            
            if (method_exists($this, $method))
            {
                $arg_count = $argc - 2;
                $arg_values = array_slice($argv, 2);
                
                call_user_func_array(array(&$this, $method), array($arg_count, $arg_values));
            }
            else
            {
                $this->showUsage();
            }
        }
    }
    
    /**
     * Tries to notify user about possible application misconfiguration errors
     */
    private function checkAppConfiguration()
    {
        // reload apps
        $this->registerApps();
        
        
        // check config
        $ret = array();
        
        foreach($this->registered_apps as $app => $config)
        {
            if (! isset($config['api_key']))
            {
                $ret[] =  "WARNING: '" . basename($app) . "' is missing API_KEY!\n";
            }

            if (! isset($config['db']))
            {
                $ret[] = "INFO: '" . basename($app) . "' is missing database configuration.\n";
            }
            else
            {
                if (! isset($config['db']['user'])
                    || !isset($config['db']['password'])
                    || !isset($config['db']['host'])
                    || !isset($config['db']['database']))
                    $ret[] = "WARNING: '" . basename($app) . "' is missing required database configuration parameters!\n";
            }
        }
        
        $this->warnings += $ret;
    }
    
    /**
     * Prints supported commands and registered app count
     */
    private function showUsage()
    {
        $app_count = count($this->registered_apps);
        
        
        echo "\nHello to Grave CLI v0.1\n";
        
        echo "\n{$app_count} app" . ($app_count == 1 ? '' : 's') . " installed.\n";
        
        echo "\nSupported commands are:\n";
        
        $commands = get_class_methods($this);
        $index = 1;
        
        foreach($commands as $command)
        {
            if (stripos($command, 'command') !== false)
            {
                echo "{$index}) " . str_replace('command', '', $command) . "\n";
                
                $index++;
            }
        }
        
        
        $this->checkAppConfiguration();
    }
    
    /**
     * Lists all available apps
     */
    private function commandList()
    {
        $app_count = count($this->registered_apps);
        
        echo "\n{$app_count} app" . ($app_count == 1 ? '' : 's') . " installed:\n";
        
        
        $index = 1;
        
        foreach($this->registered_apps as $app => $config)
        {
            echo "{$index}) {$app}\n";
            
            $index++;
        }
    }
    
    /**
     * Creates new application
     * 
     * Performs various file/directory checks and notifies user about results
     * 
     * @param int $argc
     * @param array $argv
     * @return boolean
     */
    private function commandInit($argc, $argv)
    {
        // check if app already exists
        if (array_key_exists($argv[0], $this->registered_apps)
                || is_dir($this->basePath . '/apps/' . $argv[0]))
        {
            echo "\nApp named '{$argv[0]}' already exists! Quitting.\n";
            
            return false;
        }
        
        
        // create app directory
        echo "\nCreating '{$argv[0]}'...\n";
        
        if (! is_dir($this->basePath . '/apps') || !is_writable($this->basePath . '/apps'))
        {
            echo "\nERROR: Failed to create '/apps/{$argv[0]}'. Is 'apps' writable? Quitting.\n";
            
            return false;
        }
        
        $app_root = $this->basePath . '/apps/' . str_replace(' ', '_', $argv[0]);
        
        $this->createDirectory($app_root);
        
        
        // create other neccessary directories
        $this->createDirectory("{$app_root}/assets", 0755);
        
        $this->createDirectory("{$app_root}/models", 0755);
        
        $this->createDirectory("{$app_root}/tmp", 0755);
        
        
        // generate API_KEY
        $api_key = sha1(uniqid($argv[0], true));
        
        
        // write files
        $config = $this->writeFile("{$app_root}/config.php", $this->config_template, array('KEY_PLACEHOLDER' => $api_key));
        
        $api = $this->writeFile("{$app_root}/api.php", $this->api_template);
        
        
        // see if we succeeded
        if ($config && $api)
        {
            echo "\nSuccessfully created '{$argv[0]}' with API_KEY '{$api_key}'.\n\n";
            
            echo "Don't forget to add the following HTTP headers to your requests:\n";
            echo "Content-Type: application/json\n";
            echo "X-Application-Id: " . ucwords(str_replace('_', ' ', $argv[0])) . " 1.0\n";
            echo "X-REST-API-Key: {$api_key}\n";
        }
        else
        {
            echo "\nFailed to create '{$argv[0]}'.\n";
        }
        
        
        // reload apps
        $this->checkAppConfiguration();
    }
    
    /**
     * Deletes application
     * 
     * @param int $argc
     * @param array $argv
     * @return boolean
     */
    private function commandDelete($argc, $argv)
    {
        // check if app was specified
        if ($argc < 1)
        {
            echo "\nSpecify app to delete! Quitting.\n";
            
            return false;
        }
        
        
        // check if app exists
        $directory = $this->basePath . '/apps/' . $argv[0];
        
        if (! array_key_exists($argv[0], $this->registered_apps)
                || !is_dir($directory))
        {
            echo "\nApp named '{$argv[0]}' does not exist! Quitting.\n";
            
            return false;
        }
        
        // delete all files and folders
        
        /** @var SplFileInfo[] $files */
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileinfo)
        {
            if ($fileinfo->isDir())
            {
                if (rmdir($fileinfo->getRealPath()) === false)
                {
                    echo "\nFailed to completely delete '{$argv[0]}'! Please, have a look yourself.\n";
                    
                    return false;
                }
            }
            else
            {
                if (unlink($fileinfo->getRealPath()) === false)
                {
                    echo "\nFailed to completely delete '{$argv[0]}'! Please, have a look yourself.\n";
                    
                    return false;
                }
            }
        }

        if (rmdir($directory) === false)
        {
            echo "\nFailed to completely delete '{$argv[0]}'! Please, have a look yourself.\n";
        }
        else
        {
            echo "\nSuccessfully deleted '{$argv[0]}'!\n";
        }
        
        return true;
    }
    
    /**
     * Writes template to file
     * 
     * @param string $filename
     * @param string $content
     * @param array $replacements
     * @return boolean
     */
    private function writeFile($filename, $content, $replacements = array())
    {
        if (!$handle = fopen($filename, 'w'))
        {
             echo "\nERROR: Cannot open file {$filename}\n";
             
             return false;
        }
        
        // run replacements
        if (count($replacements) > 0)
        {
            foreach($replacements as $needle => $value)
            {
                $content = str_replace($needle, $value, $content);
            }
        }
        
        if (fwrite($handle, $content) === FALSE)
        {
            echo "\nERROR: Cannot write to file {$filename}\n";

            return false;
        }
        
        fclose($handle);
        
        return true;
    }
    
    /**
     * Create directory and echo error on failure
     * 
     * @param string $path
     * @param int $mode
     * @return boolean
     */
    private function createDirectory($path, $mode = 0777)
    {
        mkdir($path, $mode);
        
        if (! is_dir($path) || !is_writable($path))
        {
            echo "\nERROR: '{$path}' is not writeable. Quitting.\n";
            
            return false;
        }
    }
    
    /**
     * Display application warnings (if any)
     */
    public function __destruct()
    {
        if (count($this->warnings) > 0)
        {
            echo "\n" . join('', $this->warnings);
        }
        
        echo "\n";
        
        fwrite(STDOUT, ob_get_clean());
    }
}