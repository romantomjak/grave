<?php
/**
 * Provides base functionality for WEB/CLI applications.
 * 
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.rst
 * Redistributions of files must retain the above copyright notice.
 *
 * @author Roman Tomjak <r.tomjaks@gmail.com>
 * @copyright 2014 Roman Tomjak
 * @link https://bitbucket.org/r00m/grave
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 * @package grave
 */

namespace Grave\Base;

class Application extends \Slim\Slim
{
    /**
     * This application's root path
     * 
     * @var string
     */
    protected $basePath;
    
    /**
     * All registered apps
     * 
     * @var array 
     */
    protected $registered_apps;
    
    /**
     * @param bool $initSlim Controls if Slim should be instantiated
     */
    public function __construct($initSlim = true)
    {
        if ($initSlim)
        {
            parent::__construct();
        }

        // set framework basepath
        $this->basePath = realpath(__DIR__ . '/../../');
        
        // load and register valid apps
        $this->registerApps();
    }
    
    /**
     * Registers all valid applications
     * 
     * @return array
     */
    protected function registerApps()
    {
        $this->registered_apps = array();

        foreach (glob($this->basePath . '/apps/*') as $app)
        {
            $config = array();
            
            if (file_exists($app . '/config.php'))
            {
                $config  = include $app . '/config.php';
                
                if (! isset($config['api_key']))
                {
                    // missing required configuration parameter: API_KEY
                    continue;
                }
            }
            
            $this->registered_apps[basename($app)] = $config;
        }
    }
    
    /**
     * Returns all, valid, app routes
     * 
     * @param class $class
     * @return array
     */
    protected function getValidRoutesForApp($class)
    {
        $ret = array();
        
        // check if valid object was passed
        if (! is_object($class))
            return $ret;
        
        // get all valid app routes
        $reflector = new \ReflectionClass($class);
        
        $methods = $reflector->getMethods(\ReflectionMethod::IS_PUBLIC);
        
        foreach($methods as $method)
        {
            // get function comment
            $docComment = $method->getDocComment();
            
            if (empty($docComment))
            {
                continue;
            }
            
            // extract params from function comment
            $regex = '#@(\S+) (\S+)#';

            if ($this->caseSensitive === false)
            {
                $regex .= 'i';
            }

            preg_match_all($regex, $docComment, $matches);
            
            if (isset($matches[1]) && isset($matches[2]))
            {
                $route = array_combine($matches[1], $matches[2]);
            }
            
            // check that all required components are present
            if (isset($route['namespace'])
                    && isset($route['url'])
                    && isset($route['via']))
            {
                // if necessary, convert "via" to array
                if (strpos($route['via'], ',') !== false)
                {
                    $route['via'] = explode(',', $route['via']);
                }
                
                $ret[$route['namespace']][] = array(
                    'url' => $route['url'],
                    'via' => $route['via'],
                    'method' => $method->name
                );
            }
        }
        
        return $ret;
    }
}