<?php
/**
 * A mix of frameworks and technologies for handling multiple API versions AND 
 * applications at the same base URL.
 * 
 * APIs are differantiated using X-Application-Id.
 * 
 * User must always specify content-type of "application/json" as well as
 * provide valid X-Application-Id and X-REST-API-Key headers for accessing
 * this API framework.
 * 
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.rst
 * Redistributions of files must retain the above copyright notice.
 *
 * @author Roman Tomjak <r.tomjaks@gmail.com>
 * @copyright 2014 Roman Tomjak
 * @link https://bitbucket.org/r00m/grave
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 * @package grave
 */

namespace Grave\Web;

class Application extends \Grave\Base\Application
{
    /**
     * Current application name (requested by client)
     * 
     * @var string
     */
    private $app_name;
    
    /**
     * Current application version (requested by client)
     * 
     * @var float
     */
    private $app_version;
    
    
    public function __construct()
    {
        parent::__construct();
        
        $this->initSlim();
        
        $this->checkRequestMediaType();
        
        $this->registerDefaultRoutes();
        
        $this->initCurrentApp();
        
        $this->registerAppRoutes();
        
        $this->enableCORS();
        
        $this->initActiveRecord();
        
        $this->run();
    }
    
    /**
     * Registers all, valid, apps routes for processing
     * 
     * This is a hack'ish way of trying to simulate Slim's "Class controller"
     * feature. It's available since 2.4.0 release.
     * 
     * @see https://github.com/codeguy/Slim/releases/tag/2.4.0
     */
    private function registerAppRoutes()
    {
        // load requested API
        require_once sprintf('%s/apps/%s/api.php', $this->basePath, $this->app_name);

        $app = new \API();

        $app->config = $this->registered_apps[$this->app_name];
        

        // register routes
        $routes = $this->getValidRoutesForApp($app);
        
        foreach($routes as $namespace => $routes)
        {
            foreach($routes as $route)
            {
                $this->group($namespace, function() use ($app, $route) {

                    $resource_url = $route['url'];
                    $class_method = $route['method'];
                    $http_methods = is_array($route['via']) ? $route['via'] : array($route['via']);
                    
                    call_user_method_array('via', $this->map($resource_url, function() use($app, $class_method) {

                        $method_reflection = new \ReflectionMethod($app, $class_method);
                        
                        $passed_arg_count = func_num_args();
                        $required_arg_count = $method_reflection->getNumberOfRequiredParameters();
                        $total_arg_count = $method_reflection->getNumberOfParameters();

                        if ($passed_arg_count < $required_arg_count
                                || $passed_arg_count > $total_arg_count)
                        {
                            $this->renderError(array(
                                'code' => 400,
                                'message' => "Invalid parameter count. Expecting {$total_arg_count}, got {$passed_arg_count}."
                            ));
                        }

                        if ($passed_arg_count > 0)
                        {
                            call_user_method_array($class_method, $app, func_get_args());
                        }
                        else
                        {
                            $app->$class_method();
                        }

                    }), $http_methods);

                });
            }
        }
    }
    
    /**
     * Whether to enable CORS headers
     */
    private function enableCORS()
    {
        $enable_cors = isset($this->registered_apps[$this->app_name]['enable_cors'])
                ? $this->registered_apps[$this->app_name]['enable_cors']
                : false;
        
        
        // register a match-all options route
        $this->options('/(:name+)', function() use ($enable_cors) {
            
            if ($enable_cors == true)
            {
                $this->response->headers['Access-Control-Allow-Origin'] = '*';
            }
            
            // if user has registered options route then allow it to execute
            if (count($this->router->getMatchedRoutes('OPTIONS', $this->router->getCurrentRoute())) > 1)
            {
                $this->pass();
            }
        });
    }
    
    /**
     * Parses HTTP headers and sets app name and version
     * 
     * @return boolean
     */
    private function checkRequestMediaType()
    {
        // don't validate requests to Grave index
        if ($this->request->getPathInfo() == '/')
            return true;
        
        // check media type
        if ($_SERVER['CONTENT_TYPE'] !== 'application/json')
            $this->renderError(array(
                'code' => 415,
                'message' => 'Have you set "Content-Type" to "application/json"?'
            ));
        
        return true;
    }
    
    /**
     * Create app and configure template rendering to use Twig
     */
    private function initSlim()
    {
        // set correct content-type
        $this->response->headers['Content-Type'] = 'application/json';
        
        
        // set debug mode
        $this->config('debug', defined('GRAVE_DEBUG'));
        
        
        // set custom router
        $this->container->singleton('router', function ($c) {
            return new \Grave\Slim\Router();
        });
        
        
        // set templating engine to Twig and configure it
        $view = $this->view(new \Slim\Views\Twig());
        
        $view->setTemplatesDirectory($this->config('templates.path'));
        
        $view->twigTemplateDirs[] = $this->basePath . '/templates/';
        
        
        // configure cache directory
        if (! is_dir($this->basePath . '/cache') || !is_writable($this->basePath . '/cache'))
        {            
            $this->renderError(array(
                'code' => 500,
                'message' => 'Cache directory missing or not writable by web server process.'
            ));
        }
        
        $view->parserOptions = array(
            'cache' => $this->basePath . '/cache',
            'auto_reload' => defined('GRAVE_DEBUG')
        );
    }
    
    /**
     * Configures active record with requested application's credentials
     */
    private function initActiveRecord()
    {
        // check config for db params
        if (isset($this->registered_apps[$this->app_name]['db']))
        {
            // setup parameters
            $model_path = $this->basePath . '/apps/' . $this->app_name . '/models/';

            $db = $this->registered_apps[$this->app_name]['db'];


            // check all required db parameters
            if (! isset($db['user'])
                    || !isset($db['password'])
                    || !isset($db['host'])
                    || !isset($db['database']))
                $this->renderError(array(
                    'code' => 500,
                    'message' => 'Missing required database configuration parameters'
                ));


            // set model dir, and connection string
            \ActiveRecord\Config::initialize(function($cfg) use($model_path, $db) {
                $cfg->set_model_directory($model_path);

                $cfg->set_connections(array(
                    'development' => 'mysql://'. $db['user'] .':'. $db['password'] .'@'. $db['host'] .'/'. $db['database'] . '; charset=utf8'
                ));
            });
        }
    }
    
    private function initCurrentApp()
    {
        // check request headers
        if (! isset($_SERVER['HTTP_X_APPLICATION_ID']) || !isset($_SERVER['HTTP_X_REST_API_KEY']))
            $this->renderError(array(
                'code' => 400,
                'message' => 'Have you set "X-Application-Id" and "X-REST-API-Key"?'
            ));

        
        // match app name and version
        if (! preg_match("#([a-zA-Z0-9\s]+)\s([\.0-9]+)#si", $_SERVER['HTTP_X_APPLICATION_ID'], $matches))
            $this->renderError(array(
                'code' => 400,
                'message' => 'Badly formated "X-Application-Id".'
            ));

        if (count($matches) !== 3)
            $this->renderError(array(
                'code' => 400,
                'message' => 'Badly formated "X-Application-Id".'
            ));


        // set values
        $this->app_name = str_replace(' ', '_', strtolower($matches[1]));
        
        $this->app_version = $matches[2];
        
        
        // check if matched app name is a registered application
        if (! array_key_exists($this->app_name, $this->registered_apps))
            $this->renderError(array(
                'code' => 404,
                'message' => 'Unknown application'
            ));
        
        
        // check api key
        if ($_SERVER['HTTP_X_REST_API_KEY'] !== $this->registered_apps[$this->app_name]['api_key'])
            $this->renderError(array(
                'code' => 401,
                'message' => 'Invalid api key'
            ));
        
        
        // check if app exists on disk
        $class = sprintf('%s/apps/%s/api.php', $this->basePath, $this->app_name);
        
        if (! file_exists($class) || !is_readable($class))
            $this->renderError(array(
                'code' => 500,
                'message' => "Missing application or it is not readable"
            ));
    }
    
    /**
     * Registers default routes - index, 404 and other errors
     */
    private function registerDefaultRoutes()
    {
        // set default index
        $this->get('/', function() {

            $this->response->headers->set('Content-Type', 'text/html');

            $this->render('index.twig');

        });
        
        // handle 404 errors
        $this->notFound(function() {

            $error = array(
                'code' => 404,
                'message' => 'Not found'
            );

            $this->renderError($error);

        });
        
        // custom error handler
        $this->error(function(\Exception $e) {
            
            $this->renderError(array(
                'code' => 500,
                'message' => 'Internal server error. Ref#' . $e->getCode()
            ));
            
        });
    }
    
    /**
     * Show error to client and stop execution
     * 
     * @param array $data
     */
    private function renderError($data)
    {
        // set correct headers for when routes are not yet defined
        if ($this->router()->getNumberOfRoutes() <= 1)
        {
            header($_SERVER['SERVER_PROTOCOL'] . ' ' . $this->response->getMessageForCode($data['code']));

            header('Content-type: application/json');
        }
        else
        {
            $this->status($data['code']);

            $this->contentType('application/json');
        }
        
        
        $this->render('error.twig', $data);
        
        $this->stop();
    }
    
}