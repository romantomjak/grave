<?php
/**
 * Exposes Slim's protected routes through a public function.
 * 
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.rst
 * Redistributions of files must retain the above copyright notice.
 *
 * @author Roman Tomjak <r.tomjaks@gmail.com>
 * @copyright 2014 Roman Tomjak
 * @link https://bitbucket.org/r00m/grave
 * @license http://opensource.org/licenses/MIT
 * @version 0.1
 * @package grave
 */

namespace Grave\Slim;

class Router extends \Slim\Router
{
    /**
     * Returns all defined routes
     * 
     * @return array
     */
    public function getRoutes()
    {
        $routes = array();
        
        if (count($this->routes) > 0)
        {
            /** @var \Slim\Route $route */
            foreach ($this->routes AS $route)
            {
                $routes[] = $route;
            }
        }
        
        return new \ArrayIterator($routes);
    }
    
    /**
     * Returns registered route count
     * 
     * @return int
     */
    public function getNumberOfRoutes()
    {
        /** @var \Slim\Route $route */
        return count($this->routes);
    }
}