=====
Grave
=====

Grave provides access to multiple APIs (and their assets) from a single base url. APIs are uniquely identified by APP_ID + AUTH_TOKEN which are both set through HTTP headers.

When requesting data from the API user must always specify content-type of "application/json" as well as provide valid X-Application-Id and X-REST-API-Key headers.

Grave itself is a mix of:

* `Slim`_
* `Twig`_
* `PHPActiveRecord`_

Goal of the project is to minimize the time spent configuring/develping API and focus on the project itself. I always find myself spending way too much time on creating standard-aware REST APIs, so this is me doing something about it.

Installation
============
Clone project and make `cache` folder world writable::

    git clone git@bitbucket.org:r00m/grave.git
    cd grave
    chmod 777 cache/

When you clone the project you already have the "helloworld" app. You can look arround or delete the folder and continue with the tutorial below.

Create app using gravec
=======================
The `gravec` is a CLI interface for creating apps. This is the preferable way to create apps! To create `my_app` simply execute::

    php gravec.php init my_app

and `gravec` will take care of creating all of the neccessary directories/files and configuration. It will even setup a stub route `<api>/v1/hello` :)

=============================
Manual configuration tutorial
=============================
If you skipped `gravec` the follow this tutorial until the end and it will teach you everything you need to know about Grave. All APIs must reside in `<grave_root>/apps/<app_name>`. So let's get started by creating a simple *hello world* app. Go ahead and create the folder inside *apps*::

    mkdir apps/helloworld

.. note ::
    Application name is determined from HTTP header `X-Application-Id`. If it contains spaces they will be replaced with underscores. Keep this in mind when creating the directory.

.. note ::
    Application ID is required so that we can differantiate between `APP1` and `APP2` API requests.

Now that the Grave knows about our app, let's create configuration for it - open `apps/helloworld/config.php` in your favorite text editor and insert the following::

    return array(
        "api_key" => sha1("my cool app") // could be anything, really
    );

API
===
Next up we must define our API. Good practice is to versionize your APIs so that changes in API wont break anything. Once again open `apps/helloworld/api.php` in your favorite text editor and copy the following into it::

    $app->group('/v1', function() use ($app) {

         require_once 'routes.php';

    });

    $app->group('/v2', function() use ($app) {

         require_once 'routes2.php';

    });

What weve done here is that weve defined two groups for our API, so that we can safely edit routes that are backed by v1 and not see any changes  in v2 and vice versa.

Routes
======
Lets continue by creating our v1 and v2 *hello* routes. Go ahead and create `apps/helloworld/routes.php`::

    // this is a route that responds to HTTP GET /hello request
    $app->get('/hello', function () use($app) {

        $ret = array(
            'hello' => 'world!'
        );

        echo json_encode($ret);

    });

and `apps/helloworld/routes2.php`::

    // this is a route that responds to HTTP GET /hello request
    $app->get('/hello', function () use($app) {

        $ret = array(
            'hello' => 'world!',
            'version' => 2
        );

        echo json_encode($ret);

    });

Client setup
============
Now that our routes are complete we can setup our API client by setting the following HTTP headers:

* Content-type: application/json
* X-Application-Id: HelloWorld v1.0
* X-REST-API-Key: 97e036e8097301fa469a1e6cc67550d7b63d7178

API key is hashed value of *my cool app* (set in config.php).

Thats it! Now we are ready to finish this tutorial by creating a request to `<api_host>/v1/hello`. You should see your data! :)

HTTP request types
==================
Grave supports GET, POST, PUT, DELETE, OPTIONS and HEAD requests. Here is an example for creating and updating records::

    $app->put('/person', function () {
        // create person
    });

    $app->post('/person/:id', function ($id) {
        // update person identified by $id
    });

For more detailed instructions visit http://docs.slimframework.com/#Routing-Overview

Database setup
==============
Modify `apps/helloworld/config.php` to look like below::

    return array(
        // api key
        'api_key' => sha1("my cool app"),

        // db credentials
        'db' => array(
            'host' => 'localhost',
            'database' => 'helloworld',
            'user' => 'helloworld',
            'password' => 'my_cool_pass',
        )
    );

Next create models folder::

    mkdir apps/helloworld/models

... and create the `apps/helloworld/models/HelloWorld.php` model::

    class HelloWorld extends ActiveRecord\Model
    {

    }

... and use it like this::

    $data = HelloWorld::all();

    foreach($items as $item)
    {
       // access database columns as $item->column_name
    }

More information on database access visit `PHPActiveRecord Basic CRUD`_

Assets
======
**WARNING: All requests to assets must contain the same HTTP headers as when making request to API!**

Lets put a nice picture of PHP elephants into our image assets, but first, lets start by creating the folders first::

    mkdir -p apps/helloworld/assets/images

then upload the image and voula! The image is accessable at `<api_host>/assets/images/<file_name>`!

License
=======

`MIT`_

.. _`Slim`: http://www.slimframework.com/
.. _`Twig`: http://twig.sensiolabs.org/
.. _`PHPActiveRecord`: http://www.phpactiverecord.com/
.. _`PHPActiveRecord Basic CRUD`: http://phpactiverecord.com/projects/main/wiki/Basic_CRUD
.. _`MIT`: http://opensource.org/licenses/MIT